RAW_BANNER_COLLAB_WARNING = """
*******************************************************************************
*******************************************************************************
**   THE COLLAB MODULE IS CURRENTLY UNDER DEVELOPMENT AND IS CONSIDERED AN   **
**   EXPERIMENTAL FEATURE. IT IS NOT RECOMMENDED FOR USE IN A PRODUCTION     **
**   ENVIRONMENT.                                                            **
*******************************************************************************
*******************************************************************************
"""

BANNER_COLLAB_WARNING = """
THE COLLAB MODULE IS CURRENTLY UNDER DEVELOPMENT AND IS CONSIDERED AN 
EXPERIMENTAL FEATURE. IT IS NOT RECOMMENDED FOR USE IN A PRODUCTION
ENVIRONMENT.
"""