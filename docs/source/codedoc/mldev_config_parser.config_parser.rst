.. automodule:: mldev_config_parser.config_parser

.. autoclass:: _MLDevSettings
   :members:
   :undoc-members:

.. autoclass:: _SingletonWrapper()
   :members:
   :special-members:
   :private-members:
   :undoc-members:
